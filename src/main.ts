import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';

import configuration from './config/configuration';
import { AppModule } from './modules/app/app.module';

const { PORT, COOKIES_SECRET } = configuration();
import cookieSession = require('cookie-session');

async function bootstrap() {
	const app = await NestFactory.create(AppModule);
	app.use(
		cookieSession({
			keys: [COOKIES_SECRET],
		}),
	);
	app.enableCors({
		origin: '*',
		credentials: true,
	});
	app.useGlobalPipes(new ValidationPipe());
	await app.listen(PORT, () => console.log(`Server started on port ${PORT}`));
}
bootstrap();
