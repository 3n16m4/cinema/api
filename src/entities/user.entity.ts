import { Field, ObjectType } from '@nestjs/graphql';

@ObjectType({ description: 'User of languages' })
export class User {
	@Field()
	user_id: string;
	@Field()
	email: string;
	@Field()
	password: string;
	@Field({ nullable: true })
	first_name: string;
	@Field({ nullable: true })
	last_name: string;
	@Field()
	last_update: string;
	@Field()
	is_subscribed: boolean;
}
