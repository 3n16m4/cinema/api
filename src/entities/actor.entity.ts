import { Field, ObjectType } from '@nestjs/graphql';

@ObjectType({ description: 'List of actors' })
export class Actor {
	@Field()
	actor_id: string;
	@Field({})
	first_name: string;
	@Field()
	last_name: string;
	@Field()
	last_update: string;
	@Field()
	img_url: string;
	@Field()
	birthday: string;
}
@ObjectType({ description: 'Response of actors' })
export class ActorsResponse {
	@Field(() => [Actor])
	data: Actor[];
	@Field()
	count: number;
}
