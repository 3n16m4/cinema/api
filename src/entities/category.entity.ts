import { Field, ObjectType } from '@nestjs/graphql';

@ObjectType({ description: 'List of categories' })
export class Category {
	@Field()
	category_id: string;
	@Field()
	category: string;
	@Field()
	last_update: string;
}

@ObjectType({ description: 'Response of categories' })
export class CategoriesResponse {
	@Field(() => [Category])
	data: Category[];
	@Field()
	count: number;
}
