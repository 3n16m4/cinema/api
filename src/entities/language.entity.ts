import { Field, ObjectType } from '@nestjs/graphql';

@ObjectType({ description: 'List of languages' })
export class Language {
	@Field()
	language_id: string;
	@Field()
	name: string;
	@Field()
	last_update: Date;
}
