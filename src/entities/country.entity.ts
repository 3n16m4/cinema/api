import { Field, ObjectType } from '@nestjs/graphql';

@ObjectType({ description: 'List of countries' })
export class Country {
	@Field()
	country_id: string;
	@Field()
	country: string;
	@Field()
	last_update: Date;
}
