import { Field, Float, ObjectType } from '@nestjs/graphql';

import { Category } from './category.entity';

@ObjectType({ description: 'Movie' })
export class Movie {
	@Field()
	movie_id: string;
	@Field()
	title: string;
	@Field()
	description: string;
	@Field()
	img_url: string;
	@Field(() => Float, { nullable: true })
	imdb: number;
	@Field(() => [Category])
	categories: Category[];
}
