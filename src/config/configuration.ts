import { config } from 'dotenv';

config({ path: `.env.${process.env.NODE_ENV}` });

export default () => ({
	PORT: parseInt(process.env.PORT, 10),
	REDIS_PORT: parseInt(process.env.REDIS_PORT, 10),
	REDIS_HOST: process.env.REDIS_HOST,
	COOKIES_SECRET: process.env.COOKIES_SECRET,
});
