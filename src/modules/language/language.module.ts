import { Module } from '@nestjs/common';

import { PrismaModule } from '../prisma/prisma.module';
import { LanguageRepository } from './language.repository';
import { LanguageResolver } from './language.resolver';
import { LanguageService } from './language.service';

@Module({
	providers: [LanguageService, LanguageResolver, LanguageRepository],
	imports: [PrismaModule],
})
export class LanguageModule {}
