import { Injectable } from '@nestjs/common';

import { LanguageRepository } from './language.repository';

@Injectable()
export class LanguageService {
	constructor(private languageRepository: LanguageRepository) {}
	getLanguages() {
		return this.languageRepository.findMany();
	}
}
