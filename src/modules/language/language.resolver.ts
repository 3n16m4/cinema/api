import { Query, Resolver } from '@nestjs/graphql';
import { Language } from 'src/entities/language.entity';

import { LanguageService } from './language.service';

@Resolver()
export class LanguageResolver {
	constructor(private languageService: LanguageService) {}
	@Query(() => [Language])
	languages() {
		return this.languageService.getLanguages();
	}
}
