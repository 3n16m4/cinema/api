import { CACHE_MANAGER } from '@nestjs/cache-manager';
import { Inject, Injectable } from '@nestjs/common';
import { Cache } from 'cache-manager';

declare global {
	interface BigInt {
		toJSON?(): string;
	}
}
BigInt.prototype.toJSON = function () {
	const int = Number.parseInt(this.toString());
	return int ?? this.toString();
};

@Injectable()
export class CacheService {
	constructor(@Inject(CACHE_MANAGER) private cacheManager: Cache) {}
	private getCacheKey(methodName: string, id: string) {
		if (id) {
			return `${methodName}_${id}`;
		}
		return methodName;
	}

	async getCache(methodName: string, params?: Record<any, any>) {
		return await this.cacheManager.get(this.getCacheKey(methodName, JSON.stringify(params)));
	}

	setCache(data: any, methodName: string, params?: Record<any, any>) {
		return this.cacheManager.set(this.getCacheKey(methodName, JSON.stringify(params)), data);
	}
	async getKeys() {
		const keys = await this.cacheManager.store.keys();
		return { keys };
	}
	async clearKey(key: string) {
		const isExists = await this.cacheManager.get(key);
		if (!isExists) {
			return { msg: 'Key not found', key: key };
		}
		await this.cacheManager.del(key);
		return { msg: 'Key removed successfully', key: key };
	}
	async clearAll() {
		await this.cacheManager.reset();
		return { msg: 'Cache cleared successfully' };
	}
}
