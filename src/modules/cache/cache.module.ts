import { CacheModule as CacheDefaultModule } from '@nestjs/cache-manager';
import { Module } from '@nestjs/common';
import { redisStore } from 'cache-manager-redis-yet';
import type { RedisClientOptions } from 'redis';

import configuration from '../../config/configuration';
import { CacheService } from './cache.service';

const { REDIS_HOST, REDIS_PORT } = configuration();

@Module({
	imports: [
		CacheDefaultModule.register<RedisClientOptions>({
			isGlobal: true,
			store: async () =>
				await redisStore({
					socket: {
						host: REDIS_HOST,
						port: REDIS_PORT,
					},
				}),
		}),
	],
	providers: [CacheService],
})
export class CacheModule {}
