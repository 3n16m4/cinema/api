import { Movie } from '@/entities/movie.entity';
import { Query, Resolver } from '@nestjs/graphql';

import { MovieService } from './movie.service';

@Resolver()
export class MovieResolver {
	constructor(private movieService: MovieService) {}

	@Query(() => [Movie])
	movies() {
		return this.movieService.getMovies();
	}
}
