import { Injectable } from '@nestjs/common';

import { MovieRepository } from './movie.repository';

@Injectable()
export class MovieService {
	constructor(private movieRepository: MovieRepository) {}

	getMovies() {
		return this.movieRepository.findMany();
	}
}
