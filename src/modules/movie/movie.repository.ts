import { Injectable } from '@nestjs/common';

import { PrismaService } from '../prisma/prisma.service';

@Injectable()
export class MovieRepository {
	constructor(private prisma: PrismaService) {}

	findMany() {
		return this.prisma.movie.findMany();
	}
}
