import { Injectable } from '@nestjs/common';

import { CountryRepository } from './country.repository';

@Injectable()
export class CountryService {
	constructor(private countryRepository: CountryRepository) {}
	getCountries() {
		return this.countryRepository.getMany();
	}
}
