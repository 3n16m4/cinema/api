import { Module } from '@nestjs/common';

import { PrismaModule } from '../prisma/prisma.module';
import { CountryRepository } from './country.repository';
import { CountryResolver } from './country.resolver';
import { CountryService } from './country.service';

@Module({
	providers: [CountryService, CountryResolver, CountryRepository],
	imports: [PrismaModule],
})
export class CountryModule {}
