import { Injectable } from '@nestjs/common';

import { PrismaService } from '../prisma/prisma.service';

@Injectable()
export class CountryRepository {
	constructor(private prisma: PrismaService) {}
	getMany() {
		return this.prisma.country.findMany();
	}
}
