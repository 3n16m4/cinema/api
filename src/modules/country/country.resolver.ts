import { Query, Resolver } from '@nestjs/graphql';
import { Country } from 'src/entities/country.entity';

import { CountryService } from './country.service';

@Resolver()
export class CountryResolver {
	constructor(private contryService: CountryService) {}
	@Query(() => [Country])
	countries() {
		return this.contryService.getCountries();
	}
}
