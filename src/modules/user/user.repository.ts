import { Injectable } from '@nestjs/common';

import { PrismaService } from '../prisma/prisma.service';
import { CreateUserArgs } from './dto/create-user.args';
import { GetUserArgs } from './dto/get-user.args';

@Injectable()
export class UserRepository {
	constructor(private prisma: PrismaService) {}
	findOne({ id, email }: GetUserArgs) {
		return this.prisma.user.findUnique({ where: { email, user_id: id } });
	}
	create(data: CreateUserArgs) {
		return this.prisma.user.create({
			data,
			select: this.prisma.prismaExclude('User', ['password']),
		});
	}
}
