import { ArgsType, Field } from '@nestjs/graphql';
import { IsEmail } from 'class-validator';

@ArgsType()
export class CreateUserArgs {
	@Field()
	@IsEmail({}, { message: 'Invalid email format' })
	email: string;
	@Field()
	password: string;
	@Field({ nullable: true })
	first_name: string;
	@Field({ nullable: true })
	last_name: string;
	@Field()
	is_subscribed: boolean;
}
