import { ArgsType, Field, ID } from '@nestjs/graphql';
import { IsEmail, IsOptional } from 'class-validator';

@ArgsType()
export class GetUserArgs {
	@Field(() => ID, { nullable: true })
	id?: string;

	@Field({ nullable: true })
	@IsOptional()
	@IsEmail({}, { message: 'Invalid email format' })
	email?: string;
}
