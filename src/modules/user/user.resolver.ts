import { User } from '@/entities/user.entity';
import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';

import { CreateUserArgs } from './dto/create-user.args';
import { GetUserArgs } from './dto/get-user.args';
import { UserService } from './user.service';

@Resolver()
export class UserResolver {
	constructor(private userService: UserService) {}
	@Query(() => User, { nullable: true })
	user(@Args() args: GetUserArgs) {
		return this.userService.getUser(args);
	}

	@Mutation(() => User)
	createUser(@Args() args: CreateUserArgs) {
		return this.userService.createUser(args);
	}
}
