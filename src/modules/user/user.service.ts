import { BadRequestException, Injectable } from '@nestjs/common';
import { scrypt as _origScrypt, randomBytes } from 'crypto';
import { promisify } from 'util';

import { CreateUserArgs } from './dto/create-user.args';
import { GetUserArgs } from './dto/get-user.args';
import { UserRepository } from './user.repository';

const scrypt = promisify(_origScrypt);

@Injectable()
export class UserService {
	constructor(private userRepository: UserRepository) {}
	private async hashPassword(password: string) {
		const salt = randomBytes(8).toString('hex');
		const buf = (await scrypt(password, salt, 64)) as Buffer;
		return `${salt}.${buf.toString('hex')}`;
	}

	async validateUser(email: string, password: string) {
		const user = await this.userRepository.findOne({ email });
		if (!user) {
			return null;
		}
		const [salt, storedHash] = user.password.split('.');
		const hash = (await scrypt(password, salt, 64)) as Buffer;
		if (storedHash !== hash.toString('hex')) {
			return null;
		}
		return user;
	}
	getUser(args: GetUserArgs) {
		return this.userRepository.findOne(args);
	}
	async createUser(args: CreateUserArgs) {
		const user = await this.userRepository.findOne({ email: args.email });
		if (user) {
			throw new BadRequestException(`User ${args.email} already exists`);
		}
		const password = await this.hashPassword(args.password);

		return this.userRepository.create({ ...args, password });
	}
}
