import { Module } from '@nestjs/common';

import { PrismaModule } from '../prisma/prisma.module';
import { ActorRepository } from './actor.repository';
import { ActorResolver } from './actor.resolver';
import { ActorService } from './actor.service';

@Module({
	providers: [ActorService, ActorRepository, ActorResolver],
	imports: [PrismaModule],
})
export class ActorModule {}
