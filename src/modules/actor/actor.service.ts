import { Injectable } from '@nestjs/common';

import { IPagination } from '../../interfaces/pagination.interface';
import { ActorRepository } from './actor.repository';

@Injectable()
export class ActorService {
	constructor(private actorRepository: ActorRepository) {}
	async getActors({ skip, take }: IPagination) {
		const actors = await this.actorRepository.findMany({ skip, take });
		const count = await this.actorRepository.getCount();
		const response = {
			data: actors,
			count,
		};
		return response;
	}
	getActor(actor_id: string) {
		return this.actorRepository.findOne(actor_id);
	}
	deleteActor(actor_id: string) {
		return this.actorRepository.delete(actor_id);
	}
}
