import { ArgsType, Field } from '@nestjs/graphql';

@ArgsType()
export class CreateActorArgs {
	@Field()
	actorId: string;
	@Field()
	first_name: string;
	@Field()
	last_name: string;
	@Field()
	birthday: Date;
}
