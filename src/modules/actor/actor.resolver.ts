import { Actor, ActorsResponse } from '@/entities/actor.entity';
import { Args, ID, Int, Mutation, Query, Resolver } from '@nestjs/graphql';

import { ActorService } from './actor.service';

// @Resolver(() => Actor)
@Resolver()
export class ActorResolver {
	constructor(private actorService: ActorService) {}
	// @UseInterceptors(UserInterceptor)
	@Query(() => ActorsResponse, { nullable: true })
	actors(
		@Args('skip', { defaultValue: 0, type: () => Int }) skip: number,
		@Args('take', { defaultValue: 10, type: () => Int }) take: number,
	) {
		return this.actorService.getActors({ skip, take });
	}
	@Query(() => Actor, { nullable: true })
	actor(@Args('actorId', { type: () => ID }) actorId: string) {
		return this.actorService.getActor(actorId);
	}

	@Mutation(() => Actor, { nullable: true })
	deleteActor(@Args('actorId', { type: () => ID }) actorId: string) {
		return this.actorService.deleteActor(actorId);
	}
}
