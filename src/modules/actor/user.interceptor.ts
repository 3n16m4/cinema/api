import { CallHandler, ExecutionContext, NestInterceptor } from '@nestjs/common';
import { Exclude, plainToClass } from 'class-transformer';
import { Observable, map } from 'rxjs';

class UserDto {
	@Exclude()
	img_url: string;
}

export class UserInterceptor implements NestInterceptor {
	intercept(
		context: ExecutionContext,
		next: CallHandler<any>,
	): Observable<any> | Promise<Observable<any>> {
		return next.handle().pipe(
			map((data: any) => {
				return plainToClass(UserDto, data, {
					excludeExtraneousValues: true,
				});
			}),
		);
	}
}
