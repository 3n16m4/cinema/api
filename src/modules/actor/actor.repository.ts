import { BadRequestException, Injectable } from '@nestjs/common';

import { IPagination } from '../../interfaces/pagination.interface';
import { PrismaService } from '../prisma/prisma.service';
import { CreateActorArgs } from './dto/create-actor.args';

@Injectable()
export class ActorRepository {
	constructor(private prisma: PrismaService) {}
	findMany({ skip, take }: IPagination) {
		return this.prisma.actor.findMany({ skip, take });
	}
	findOne(actor_id: string) {
		return this.prisma.actor.findUnique({ where: { actor_id } });
	}
	async delete(actor_id: string) {
		const actor = await this.findOne(actor_id);
		if (!actor) {
			throw new BadRequestException('Actor does not exist');
		}
		return this.prisma.actor.delete({ where: { actor_id } });
	}
	async create(data: CreateActorArgs) {
		return this.prisma.actor.create({
			data: {
				...data,
				img_url: '',
			},
		});
	}
	getCount() {
		return this.prisma.actor.count();
	}
}
