import { ApolloDriver, ApolloDriverConfig } from '@nestjs/apollo';
import { Module } from '@nestjs/common';
import { GraphQLModule } from '@nestjs/graphql';
import { join } from 'path';

import { ActorModule } from '../actor/actor.module';
import { CategoryModule } from '../category/category.module';
import { UserModule } from '../user/user.module';

@Module({
	imports: [
		GraphQLModule.forRoot<ApolloDriverConfig>({
			driver: ApolloDriver,
			path: '/graphql',
			autoSchemaFile: join(process.cwd(), 'src/schema.gql'),
		}),
		CategoryModule,
		ActorModule,
		UserModule,
	],
})
export class AppModule {}
