import { Injectable } from '@nestjs/common';

import { CategoryRepository } from './category.repository';
import { CreateCategoryArgs } from './dto/create-category.args';
import { DeleteCategoryArgs } from './dto/delete-category.args';
import { EditCategoryArgs } from './dto/edit-category.args';

@Injectable()
export class CategoryService {
	constructor(private categoryRepository: CategoryRepository) {}
	async getCategories() {
		const categories = await this.categoryRepository.findMany();
		const countOfCategories = await this.categoryRepository.getCount();

		const response = {
			data: categories,
			count: countOfCategories,
		};
		return response;
	}
	async getCategory(id: string) {
		const response = await this.categoryRepository.findOne(id);
		if (response) {
		}

		return response;
	}

	createCategory(category: CreateCategoryArgs) {
		return this.categoryRepository.create(category);
	}
	deleteCategory(category: DeleteCategoryArgs) {
		return this.categoryRepository.delete(category);
	}
	editCategory(category: EditCategoryArgs) {
		return this.categoryRepository.edit(category);
	}
}
