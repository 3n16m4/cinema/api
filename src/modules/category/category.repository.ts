import { BadRequestException, Injectable } from '@nestjs/common';

import { PrismaService } from '../prisma/prisma.service';
import { CreateCategoryArgs } from './dto/create-category.args';
import { DeleteCategoryArgs } from './dto/delete-category.args';
import { EditCategoryArgs } from './dto/edit-category.args';

@Injectable()
export class CategoryRepository {
	constructor(private prisma: PrismaService) {}
	findMany() {
		return this.prisma.category.findMany();
	}
	findOne(category_id: string) {
		return this.prisma.category.findUnique({ where: { category_id } });
	}

	async findByName(category: string) {
		const data = await this.prisma.category.findUnique({ where: { category } });
		return data;
	}

	async create(data: CreateCategoryArgs) {
		const category = await this.findByName(data.category);
		if (category) {
			throw new BadRequestException('Category already exists');
		}
		return this.prisma.category.create({ data });
	}
	async delete(data: DeleteCategoryArgs) {
		const category = await this.findByName(data.id);
		if (!category) {
			throw new BadRequestException('Category does not exist');
		}
		return this.prisma.category.delete({ where: { category_id: data.id } });
	}
	async edit(data: EditCategoryArgs) {
		const category = await this.prisma.category.findUnique({ where: { category_id: data.id } });
		if (!category) {
			throw new BadRequestException('Category does not exist');
		}
		return this.prisma.category.update({
			where: { category_id: data.id },
			data: { category: data.category },
		});
	}
	getCount() {
		return this.prisma.category.count();
	}
}
