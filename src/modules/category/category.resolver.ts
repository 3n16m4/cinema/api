import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';

import { CategoriesResponse, Category } from '../../entities/category.entity';
import { CategoryService } from './category.service';
import { CreateCategoryArgs } from './dto/create-category.args';
import { DeleteCategoryArgs } from './dto/delete-category.args';
import { EditCategoryArgs } from './dto/edit-category.args';
import { GetCategoryArgs } from './dto/get-category.args';

@Resolver()
export class CategoryResolver {
	constructor(private categoryService: CategoryService) {}
	@Query(() => CategoriesResponse, { nullable: true })
	categories() {
		return this.categoryService.getCategories();
	}
	@Query(() => Category, { nullable: true })
	category(@Args() args: GetCategoryArgs) {
		return this.categoryService.getCategory(args.id);
	}

	@Mutation(() => Category)
	createCategory(@Args() args: CreateCategoryArgs) {
		return this.categoryService.createCategory(args);
	}
	@Mutation(() => Category, { nullable: true })
	deleteCategory(@Args() args: DeleteCategoryArgs) {
		return this.categoryService.deleteCategory(args);
	}
	@Mutation(() => Category)
	editCategory(@Args() args: EditCategoryArgs) {
		return this.categoryService.editCategory(args);
	}
}
