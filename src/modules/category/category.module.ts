import { Module } from '@nestjs/common';

import { PrismaModule } from '../prisma/prisma.module';
import { CategoryRepository } from './category.repository';
import { CategoryResolver } from './category.resolver';
import { CategoryService } from './category.service';

@Module({
	providers: [CategoryService, CategoryResolver, CategoryRepository],
	imports: [PrismaModule],
})
export class CategoryModule {}
