import { ArgsType, Field, ID } from '@nestjs/graphql';

@ArgsType()
export class DeleteCategoryArgs {
	@Field(() => ID)
	id: string;
}
