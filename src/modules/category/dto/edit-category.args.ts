import { ArgsType, Field, ID } from '@nestjs/graphql';

@ArgsType()
export class EditCategoryArgs {
	@Field(() => ID)
	id: string;
	@Field()
	category: string;
}
