import { faker } from '@faker-js/faker';
import { PrismaClient } from '@prisma/client';
import { countries } from 'countries-list';

const prisma = new PrismaClient();

const MOVIE_CATEGORIES = [
	'Action',
	'Comedy',
	'Drama',
	'Science Fiction',
	'Fantasy',
	'Horror',
	'Romance',
	'Thriller',
	'Documentary',
	'Adventure',
];
const LANGUAGES = [
	'English',
	'Spanish',
	'French',
	'German',
	'Chinese',
	'Japanese',
	'Korean',
	'Arabic',
	'Russian',
	'Portuguese',
	'Ukrainian',
];

async function main() {
	const listCountries = Object.values(countries).map((country) => ({
		country_id: faker.string.uuid(),
		country: country.name,
	}));
	const listCategories = MOVIE_CATEGORIES.map((category) => ({
		category_id: faker.string.uuid(),
		category,
	}));
	const listLanguages = LANGUAGES.map((language) => ({
		language_id: faker.string.uuid(),
		language,
	}));
	const listActors = Array.from(Array(1000).keys()).map(() => ({
		actor_id: faker.string.uuid(),
		first_name: faker.person.firstName(),
		last_name: faker.person.lastName(),
		img_url: faker.image.avatar(),
		birthday: faker.date.birthdate(),
	}));
	const listMovies = Array.from(Array(10000).keys()).map(() => ({
		movie_id: faker.string.uuid(),
		title: `${faker.lorem.word()} ${faker.lorem.word()}`,
		description: faker.lorem.paragraph(),
		img_url: faker.image.url(),
		imdb: faker.number.float({ min: 0, max: 10, precision: 0.1 }),
	}));

	await prisma.$transaction([
		prisma.category.deleteMany(),
		prisma.actor.deleteMany(),
		prisma.country.deleteMany(),
		prisma.language.deleteMany(),
		prisma.movie.deleteMany(),
	]);

	await prisma.category.createMany({
		data: listCategories,
	});
	await prisma.actor.createMany({
		data: listActors,
	});
	await prisma.country.createMany({
		data: listCountries,
	});
	await prisma.language.createMany({
		data: listLanguages,
	});
	await prisma.movie.createMany({
		data: listMovies,
	});
}
main()
	.then(async () => {
		await prisma.$disconnect();
	})
	.catch(async (e) => {
		console.error(e);
		await prisma.$disconnect();
		process.exit(1);
	});
