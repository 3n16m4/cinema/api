-- CreateTable
CREATE TABLE "actor" (
    "actor_id" TEXT NOT NULL,
    "first_name" VARCHAR(45) NOT NULL,
    "last_name" VARCHAR(45) NOT NULL,
    "last_update" TIMESTAMP(6) NOT NULL DEFAULT CURRENT_TIMESTAMP,

    CONSTRAINT "actor_pkey" PRIMARY KEY ("actor_id")
);

-- CreateTable
CREATE TABLE "country" (
    "country_id" TEXT NOT NULL,
    "country" VARCHAR(45) NOT NULL,
    "last_update" TIMESTAMP(6) NOT NULL DEFAULT CURRENT_TIMESTAMP,

    CONSTRAINT "country_pkey" PRIMARY KEY ("country_id")
);

-- CreateTable
CREATE TABLE "language" (
    "language_id" TEXT NOT NULL,
    "language" VARCHAR(45) NOT NULL,

    CONSTRAINT "language_pkey" PRIMARY KEY ("language_id")
);

-- CreateTable
CREATE TABLE "category" (
    "category_id" TEXT NOT NULL,
    "category" VARCHAR(45) NOT NULL,
    "last_update" TIMESTAMP(6) NOT NULL DEFAULT CURRENT_TIMESTAMP,

    CONSTRAINT "category_pkey" PRIMARY KEY ("category_id")
);

-- CreateTable
CREATE TABLE "movie" (
    "movie_id" TEXT NOT NULL,
    "title" VARCHAR(45) NOT NULL,
    "description" VARCHAR NOT NULL,
    "img_url" VARCHAR NOT NULL,
    "imdb" DOUBLE PRECISION NOT NULL,

    CONSTRAINT "movie_pkey" PRIMARY KEY ("movie_id")
);

-- CreateTable
CREATE TABLE "category_movie" (
    "id" TEXT NOT NULL,
    "category_id" TEXT NOT NULL,
    "movie_id" TEXT NOT NULL,

    CONSTRAINT "category_movie_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "category_category_key" ON "category"("category");

-- AddForeignKey
ALTER TABLE "category_movie" ADD CONSTRAINT "category_movie_category_id_fkey" FOREIGN KEY ("category_id") REFERENCES "category"("category_id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "category_movie" ADD CONSTRAINT "category_movie_movie_id_fkey" FOREIGN KEY ("movie_id") REFERENCES "movie"("movie_id") ON DELETE RESTRICT ON UPDATE CASCADE;
