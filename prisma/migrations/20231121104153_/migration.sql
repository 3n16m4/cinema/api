/*
  Warnings:

  - Added the required column `img_url` to the `actor` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "actor" ADD COLUMN     "birthday" TIMESTAMP(3),
ADD COLUMN     "img_url" VARCHAR NOT NULL;
