/*
  Warnings:

  - Added the required column `is_subscribed` to the `user` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "user" ADD COLUMN     "is_subscribed" BOOLEAN NOT NULL,
ADD COLUMN     "last_update" TIMESTAMP(6) NOT NULL DEFAULT CURRENT_TIMESTAMP;
