# API

```bash
npx prettier . --write
```

### Pool database

```bash
dotenv -e .env.development npx prisma db pull
```

### Running migrates

```bash
dotenv -e .env.development npx prisma migrate dev --name init
```

### Running seed

```bash
dotenv -e .env.development npx prisma db seed
```
